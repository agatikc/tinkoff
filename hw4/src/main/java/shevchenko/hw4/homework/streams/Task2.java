package shevchenko.hw4.homework.streams;

import shevchenko.hw4.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task2 {
    private final Set<User> users = new HashSet<>();

    public int getTotalAge() {
        // ������������� ���������� ������� ������������ �� ������������������ ������.
        return users.stream()
            .mapToInt(User::getAge)
            .sum();
    }

    // ����������, ���������� ������������������ �����.
    public int countEmployees(Map<String, List<User>> departments) {
        return departments.values().stream()
            .mapToInt(List::size)
            .sum();
    }
}
