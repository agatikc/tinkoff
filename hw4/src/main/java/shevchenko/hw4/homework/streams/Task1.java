package shevchenko.hw4.homework.streams;

import java.util.stream.Collectors;
import shevchenko.hw4.User;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

public class Task1 {

    // ������������ Collector � Map, ������������ � �������� ���������� ������������� �������.
    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {
        return users.stream()
            .collect(Collectors.toMap(User::getName, User::getAge, Integer::max));
    }
}
