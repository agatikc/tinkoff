package shevchenko.hw4.homework.streams;


import java.util.IntSummaryStatistics;
import shevchenko.hw4.User;

import java.util.List;
import java.util.stream.IntStream;

public class Task3 {
    public void printNameStats(List<User> users) {
        // ��������, �� ����� �������� �� ������ ������������ � ��������, � �������.
        // � ��� ����� �� ��������� 2 ���� ���� � ��� �� �����.
        // ���� IntStream ��������, �� � ��� ���� ������������ � ����������� ��������.
        IntStream stream = getNameLengthStream(users);
        if (stream.findAny().isPresent()) {
            IntSummaryStatistics stats = getNameLengthStream(users).summaryStatistics();
            System.out.println("MAX: " + stats.getMax());
            System.out.println("MIN: " + stats.getMin());
        }
    }

    private IntStream getNameLengthStream(List<User> users) {
        return users.stream()
                .mapToInt(user -> user.getName().length());
    }
}
