package shevchenko.hw4.homework.optional;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Task1 {
    public List<Email> create() {
        // ������� �� ���������� �������� � ������ ��� ��������.
        Email noAttachment = new Email("First!", "No attachment");
        Attachment attachment = new Attachment("/mnt/files/image.png", 370);
        // ������� ���������� ������ ��������, ���� ���� ��� null.
        Email withAttachment = new Email("Second!", "With attachment", attachment);
        return Arrays.asList(noAttachment, withAttachment);
    }

    // ������� Email static. ������ ��� ��� �� ����� ���������� ������ � ������ ���������� ������.
    static class Email implements Serializable {
        private final String subject;
        private final String body;

        //Optional ����� �� ������������ � �������� ���� ��� ���������.
        //�� ������ ����� �� ����� ��������.
        private final Attachment attachment;

        Email(String subject, String body, Attachment attachment) {
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
        }

        // ����������� ��� �������� ������ ��� ��������.
        Email(String subject, String body) {
            this(subject, body, null);
        }

        String getSubject() {
            return subject;
        }

        String getBody() {
            return body;
        }

        // ���������� ��������, �� ��� ����� � �� ����, ������� Optional.
        Optional<Attachment> getAttachment() {
            return Optional.ofNullable(attachment);
        }
    }

    // ������� Attachment -> record. ������ ������������. �� ���������� ������������.
    record Attachment(String path, int size){}
}
