package shevchenko.hw4.homework.optional;

import java.util.Collections;
import java.util.NoSuchElementException;
import shevchenko.hw4.User;

import java.util.List;
import java.util.Optional;

public class Task2 {
    private static final String ADMIN_ROLE = "admin";

    // ������� ����� ��� ���� ������� Optional.
    public Optional<User> findAnyAdmin() {
        Optional<List<User>> users = findUsersByRole(ADMIN_ROLE);
        // ����� ������� �� Optional ������ � ����� ����� ��� �������.
        // findAny ����� ����������, ���� ������ null. � ������ ��� ���������� �� �� �����.
        return users.orElse(Collections.emptyList()).stream().findAny();
    }

    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }
}
