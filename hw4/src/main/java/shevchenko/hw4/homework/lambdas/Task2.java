package shevchenko.hw4.homework.lambdas;

import shevchenko.hw4.User;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public class Task2 {
    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    public void addUser(User user) {
        // ��������� ���������� ������ ���� �� ������ Set �� ����. ���� ���� ��� ������ ������.
        user.getRoles().forEach(role -> {
            Set<User> usersInRole = usersByRole.getOrDefault(role.getName(), new HashSet<>());
            usersInRole.add(user);
            // ��������, ��� �� ��������� ��� � ����� ������.
            // ����, ��������, getOrDefault ���������� ������ �� Set, � ������� ������������� ����������� usersByRole.
            usersByRole.put(role.getName(), usersInRole);
        });
    }

    public Set<User> getUsersInRole(String role) {
        // ������������� getOrDefault, ����� �� ������ �������� �������.
        return usersByRole.getOrDefault(role, Collections.emptySet());
    }

}
