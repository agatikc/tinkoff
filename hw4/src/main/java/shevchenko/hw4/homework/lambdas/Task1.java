package shevchenko.hw4.homework.lambdas;

import java.util.Collections;
import shevchenko.hw4.Permission;
import shevchenko.hw4.User;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Task1 {
    private final Set<User> users = new HashSet<>();

    public void removeUsersWithPermission(Permission permission) {
        // ������� � �������������� ����� � ������������� ���������� �������.
        users.removeIf(user -> userHasPermission(user, permission));
    }

    // ����� ��� �������� ����, ���� �� � ������������ ����������� ����������. Naming.
    boolean userHasPermission (User user, Permission permission) {
        return user.getRoles().stream()
            .anyMatch(role -> role.getPermissions().contains(permission));
    }
}