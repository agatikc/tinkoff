package shevchenko.hw4;

public enum Permission {
    ADD, EDIT, SEARCH, DELETE
}
