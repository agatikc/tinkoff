package shevchenko.hw1;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task2 {
    public static Map<Integer, Integer> toFrequencyMap(List<Integer> list) {
        return list.stream()
                .collect(Collectors.toMap(e -> e,
                        e -> 1, Integer::sum))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue));
    }

    public static void main(String[] args) {
        test(100);
        test(150);
        test(1000);
    }

    private static void test(int size) {
        Random rnd = new Random();
        List<Integer> testList = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList());
        for (int i = 0; i < size - 100; i++) {
            testList.add(rnd.nextInt(1, 101));
        }
        Collections.shuffle(testList);
        System.out.println(toFrequencyMap(testList));
    }
}
