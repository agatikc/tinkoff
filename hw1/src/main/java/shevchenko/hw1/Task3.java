package shevchenko.hw1;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.IntStream;

public class Task3 {
    private static Store store;
    private static final Random rnd = new Random();
    private static class Store {
        private final Queue<Object> product = new ArrayBlockingQueue<>(256, true);
        public void put(int amount) {
            synchronized (product) {
                for (int i = 0; i < amount; i++) {
                    product.offer(new Object());
                }
                System.out.println("Producer delivered " + amount + ".");
                product.notify();
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        public void get(String consumer) {
            synchronized (product) {
                while (product.isEmpty()) {
                    try {
                        System.out.println(consumer + " waits product.");
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                product.poll();
                System.out.println(consumer + " gets product.");
            }
        }
    }
    private record Producer(int numberOfDeliveries) implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < numberOfDeliveries; i++) {
                store.put(rnd.nextInt(1,5));
            }
        }
    }
    private record Consumer(int amount) implements Runnable{
        @Override
        public void run() {
            store.get(Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {

        store = new Store();
        Thread producer = new Thread(new Producer(10));
        Thread[] consumers = IntStream.range(0, 10)
                .mapToObj((id) -> new Thread(new Consumer(/*rnd.nextInt(1,5)*/1)))
                .toArray(Thread[]::new);
        producer.start();
        Arrays.stream(consumers).forEach(Thread::start);
        //want to have bigger delivery, than consumers want
    }
}

