package shevchenko.hw8;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.List;

public interface BookFileReader {

  String AUTHOR_PREFIX = "Author: ";
  String TITLE_PREFIX = "Title: ";

  List<Book> parse(Path bookFilePath) throws IOException, ParseException;

  static TimeStatistic takeTimeStatistic(BookFileReader reader, Path path, boolean hasLog,
      int iterations) {
    final long startTime = System.currentTimeMillis();
    final Runtime runtime = Runtime.getRuntime();
    long memory = 0;
    List<Book> books;
    for (int i = 0; i < iterations; i++) {
      try {
        System.gc();
        memory += runtime.totalMemory() - runtime.freeMemory();
        books = reader.parse(path);
        if (hasLog) {
          System.out.println(books);
        }
      } catch (IOException | ParseException e) {
        System.err.println(e.getMessage());
      }
    }
    final long endTime = System.currentTimeMillis();
    return new TimeStatistic(reader.getClass().getSimpleName(),
        (endTime - startTime) / iterations, toKB(memory / iterations), iterations);
  }

  private static long toKB(long l) {
    return l/1_000;
  }

  default String getTitle(String line) {
    return line.substring(TITLE_PREFIX.length());
  }

  default String getAuthor(String line) {
    return line.substring(AUTHOR_PREFIX.length());
  }

  default boolean isTitleInfo(String line) {
    return isInfoWithPrefix(line, TITLE_PREFIX);
  }

  default boolean notAuthorInfo(String line) {
    return !isInfoWithPrefix(line, AUTHOR_PREFIX);
  }

  default boolean isAuthorInfo(String line) {
    return isInfoWithPrefix(line, AUTHOR_PREFIX);
  }

  private boolean isInfoWithPrefix(String line, String prefix) {
    return line.startsWith(prefix);
  }

  static TimeStatistic takeTimeStatistic(BookFileReader reader, Path path, boolean hasLog) {
    return takeTimeStatistic(reader, path, hasLog, 10);
  }
}
