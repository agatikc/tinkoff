package shevchenko.hw8;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

  private final static String FILE_PATH = "FILE_PATH";

  public static void main(String[] args) {
    final Path booksFilePath;
    try {
      final String bookPath = System.getenv(FILE_PATH);
      try {
        booksFilePath = Paths.get(bookPath);
        final boolean hasLog = false;

        TimeStatistic blocking = BookFileReader.takeTimeStatistic(new BlockingBookFileReader(),
            booksFilePath, hasLog);
        System.out.println("Не реактивный подход: " + blocking);

        boolean hasSequencedTask = true;
        TimeStatistic slowReactive = BookFileReader.takeTimeStatistic(
            new ReactiveBookFileReader(hasSequencedTask),
            booksFilePath, hasLog);
        System.out.println(
            "Реактивный подход с последовательным действием (с collectList): " + slowReactive);

        TimeStatistic fastReactive = BookFileReader.takeTimeStatistic(
            new ReactiveBookFileReader(!hasSequencedTask),
            booksFilePath, hasLog);
        System.out.println(
            "Реактивный подход без последовательного действия (без collectList): " + fastReactive);
      } catch (InvalidPathException e) {
        System.err.println(e.getMessage());
      }
    } catch (InvalidPathException e) {
      System.err.println(e.getMessage());
    }
  }
}
