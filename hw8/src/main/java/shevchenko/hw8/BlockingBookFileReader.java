package shevchenko.hw8;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class BlockingBookFileReader implements BookFileReader {

  @Override
  public List<Book> parse(Path bookFilePath) throws IOException, ParseException {
    try (BufferedReader reader = Files.newBufferedReader(bookFilePath)) {
      List<Book> bookList = new ArrayList<>();
      String line;
      while ((line = reader.readLine()) != null) {
        if (isTitleInfo(line)) {
          StringBuilder titleBuilder = new StringBuilder(getTitle(line));
          while ((line = reader.readLine()) != null && notAuthorInfo(line)) {
            titleBuilder.append(line);
          }
          if (line == null) {
            throw new ParseException("Can't find author.", 0);
          }
          String author = getAuthor(line);
          String title = titleBuilder.toString();
          bookList.add(new Book(title, author));
        }
      }
      return bookList;
    }
  }

}
