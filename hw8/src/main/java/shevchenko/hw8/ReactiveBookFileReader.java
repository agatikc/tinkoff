package shevchenko.hw8;

import java.nio.file.Path;
import java.util.List;
import org.javaync.io.AsyncFiles;
import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;

public class ReactiveBookFileReader implements BookFileReader {
  private final boolean hasSequancedTask;

  public ReactiveBookFileReader(boolean hasSequencedTask) {
    this.hasSequancedTask = hasSequencedTask;
  }

  @Override
  public List<Book> parse(Path bookFilePath) {
    Flux<String> lines = Flux.from(AsyncFiles.lines(bookFilePath));
    ParallelFlux<Flux<String>> bookFluxes = lines
        .windowUntil(this::isTitleInfo, true)
        .parallel(8)
        .runOn(Schedulers.parallel());
    ParallelFlux<Book> books = bookFluxes
        .map(bookFlux -> bookFlux.filter(this::isTitleOrAuthor)
            .map(this::toTitleOrAuthor))
        .flatMap(Book::toBookFlux);
    if (hasSequancedTask) {
      return books.sequential().collectList().block();
    } else {
      return null;
    }
  }

  private String toTitleOrAuthor(String line) {
    return isTitleInfo(line) ? getTitle(line)
        : (isAuthorInfo(line) ? getAuthor(line) : "??");
  }
  private boolean isTitleOrAuthor(String line) {
    return isTitleInfo(line) || isAuthorInfo(line);
  }
}
