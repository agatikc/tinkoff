package shevchenko.hw8;

public record TimeStatistic(String type, long time, long memory, int iterations) {

  @Override
  public String toString() {
    return String.format(
        "%s: average time - %d ms per iteration, average memory - %dKB per iteration, iters - %d.\n",
        type, time, memory, iterations);
  }
}
