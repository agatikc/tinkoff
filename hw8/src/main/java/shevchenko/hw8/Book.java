package shevchenko.hw8;

import java.util.List;
import reactor.core.publisher.Flux;

public class Book {

  private final String title, author;

  public Book(String title, String author) {
    this.author = author;
    this.title = title;
  }


  @Override
  public String toString() {
    return author + ": " + title;
  }

  public Book(List<String> pair) {
    this.author = pair.get(1);
    this.title = pair.get(0);
  }

  public static Flux<Book> toBookFlux(Flux<String> books) {
    return books.buffer(2).map(Book::new);
  }
}
