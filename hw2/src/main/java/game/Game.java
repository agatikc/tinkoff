package game;

import board.TicTacToeBoard;
import players.Player;

public class Game {
    private final GameState gameState;
    private final Player firstPlayer, secondPlayer;
    public Game(final Player firstPlayer, final Player secondPlayer) {
        this.gameState = new GameState();
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }
    public void play(TicTacToeBoard board) {
        while (true) {
            final Result res = makeMove(board, gameState);
            if (res != Result.CONTINUE && res != Result.AGAIN)
                break;
            if(res != Result.AGAIN) gameState.changeTurn();
        }
    }

    private Player getPlayer(GameState gameState) {
        return gameState.isFirstPlayerMove() ? firstPlayer : secondPlayer;
    }

    private Result makeMove(final TicTacToeBoard board, final GameState turn) {
        final Player player = getPlayer(turn);
        final Move move = player.move(turn, board);
        final Result res = board.makeMoveAndChangePosition(move);
        log("Player " + player.getName() + ": " + move + ".");
        log("Position:\n" + board);
        log(resToString(res, player));
        return res;
    }

    private String resToString(Result res, Player player) {
        return switch (res) {
            case AGAIN -> "Wrong move. Try again!";
            case WIN -> "Player " + player.getName() + " won.";
            case DRAW -> "Draw.";
            case CONTINUE -> "Next move.";
        };
    }

    private void log(final String message) {
            System.out.println(message);
    }
}
