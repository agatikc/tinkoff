package game;

public enum Result {
  CONTINUE, AGAIN, WIN, DRAW
}
