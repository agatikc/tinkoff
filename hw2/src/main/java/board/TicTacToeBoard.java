package board;

import game.GameState;
import game.Move;
import game.Result;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import cell.Cell;
import cell.CellState;

public class TicTacToeBoard implements Board {

  private final int size;
  private final Cell[][] cells;

  public TicTacToeBoard(int size) {
    this.size = size;
    this.cells = initCells();
  }

  public TicTacToeBoard(Position position) {
    Cell[][] posCells = position.getPositionAsArray();
    size = posCells.length;
    cells = initCells(posCells);
  }

  private Cell[][] initCells() {
    Cell[][] res = new Cell[size][size];
    for (Cell[] row : res) {
      Arrays.setAll(row, e -> new Cell());
    }
    return res;
  }

  private Cell[][] initCells(Cell[][] posCells) {
    Cell[][] res = new Cell[size][];
    for (int i = 0; i < size; i++) {
      cells[i] = new Cell[size];
      for (int j = 0; j < size; j++) {
        cells[i][j] = posCells[i][j].clone();
      }
    }
    return res;
  }

  @Override
  public Result makeMoveAndChangePosition(Move move) {
    if (!isValid(move)) {
      return Result.AGAIN;
    }
    cells[move.getRow()][move.getColumn()]
        .setState(move.getValue());
    return analysePositionAndGetResult(move.getValue());
  }

  @Override
  public Result analysePositionAndGetResult(CellState current) {
    int empty = 0, inDiag = 0, inBackDiag = 0;
    for (int i = 0; i < size; i++) {
      int inRow = 0, inColumn = 0;
      for (int j = 0; j < size; j++) {
          if (cells[i][j].hasState(current)) {
              inRow++;
          }
          if (cells[j][i].hasState(current)) {
              inColumn++;
          }
          if (cells[i][j].isEmpty()) {
              empty++;
          }
      }
      if (inRow == size || inColumn == size) {
        return Result.WIN;
      }
        if (cells[i][i].hasState(current)) {
            inDiag++;
        }
        if (cells[i][size - 1 - i].hasState(current)) {
            inBackDiag++;
        }
    }
    if (inDiag == size || inBackDiag == size) {
      return Result.WIN;
    }
    if (empty == 0) {
      return Result.DRAW;
    }
    return Result.CONTINUE;
  }

  @Override
  public boolean isValid(Move move) {
    int row = move.getRow(), column = move.getColumn();
    return row >= 0  && row < size
        && column >= 0  && column < size
        && cells[row][column].isEmpty();
  }

  @Override
  public Cell[][] getPositionAsArray() {
    return cells;
  }

  @Override
  public Move[] getPossibleMoves(GameState state) {
    //TODO: unefficient
    List<Move> possibleMoves = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (cells[i][j].isEmpty()) {
          possibleMoves.add(new Move(i, j, state));
        }
      }
    }
    Collections.shuffle(possibleMoves);
    return possibleMoves.toArray(Move[]::new);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(" ");
    sb.append(IntStream.range(0, size)
        .mapToObj(Integer::toString)
        .collect(Collectors.joining("")));
    for (int r = 0; r < size; r++) {
      sb.append("\n");
      sb.append(r);
      for (int c = 0; c < size; c++) {
        sb.append(cells[r][c]);
      }
    }
    return sb.toString();
  }

}
