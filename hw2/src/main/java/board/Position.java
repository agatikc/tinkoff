package board;

import game.GameState;
import game.Move;
import game.Result;
import cell.Cell;
import cell.CellState;

public interface Position {

  Result makeMoveAndChangePosition(Move move);

  Result analysePositionAndGetResult(CellState current);

  boolean isValid(Move move);

  Cell[][] getPositionAsArray();

  Move[] getPossibleMoves(GameState state);
}
