package shevchenko.hw2.players;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import shevchenko.hw2.GameState;
import shevchenko.hw2.Move;
import shevchenko.hw2.Result;
import shevchenko.hw2.board.Position;
import shevchenko.hw2.board.TicTacToeBoard;

public class MinimaxPlayer extends Player {

  private final PrintStream out;
  private final Map<Position, MinimaxResult> minimaxCache = new HashMap<>();
  int maxDepth;

  public MinimaxPlayer(final String name, final PrintStream out, int depth) {
    super(name);
    this.out = out;
    this.maxDepth = depth;
  }

  public MinimaxPlayer(final String name, int depth) {
    this(name, System.out, depth);
  }

  public MinimaxPlayer(final String name) {
    this(name, Integer.MAX_VALUE);
  }

  @Override
  public Move move(GameState gameState, Position position) {
    out.println(getName() + "'s move.");
    out.println("Enter row and column");
    MinimaxResult result = minimax(gameState, position, gameState, 0);
    return result.move;
  }

  MinimaxResult minimax(GameState currentGameState,
      Position currentPosition,
      GameState startGameState,
      int depth) {
    if (depth >= maxDepth) {
      return new MinimaxResult(null, 0, currentGameState, depth);
    }
    if (positionCached(currentPosition, currentGameState)) {
      return minimaxCache.get(currentPosition);
    }
    SortedSet<MinimaxResult> minimaxResultSet =
        getChildrenResultSet(currentPosition,
            startGameState, currentGameState, depth);

    MinimaxResult result;
      if (currentGameState == startGameState) {
          result = minimaxResultSet.last();
      } else {
          result = minimaxResultSet.first();
      }

    minimaxCache.put(currentPosition, result);
    return result;
  }

  private SortedSet<MinimaxResult> getChildrenResultSet(Position currentPosition,
      GameState startGameState, GameState currentGameState, int depth) {
    final SortedSet<MinimaxResult> minimaxResultSet = new TreeSet<>();
    for (Move possibleMove : currentPosition.getPossibleMoves(currentGameState)) {
      Position newPosition = new TicTacToeBoard(currentPosition);
      Result result = newPosition.makeMoveAndChangePosition(possibleMove);

      MinimaxResult minimaxResult = getMinimaxResult(result, possibleMove,
          startGameState, currentGameState, newPosition, depth);
      minimaxResultSet.add(minimaxResult);
    }
    return minimaxResultSet;
  }

  private MinimaxResult getMinimaxResult(Result result, Move possibleMove, GameState startGameState,
      GameState currentGameState, Position newPosition, int depth) {
    int score = 0, winDepth = depth;
    switch (result) {
      case CONTINUE -> {
        GameState newGameState = new GameState(currentGameState);
        newGameState.changeTurn();
        MinimaxResult minimaxDeeperResult = minimax(newGameState,
            newPosition, startGameState, depth + 1);
        score = minimaxDeeperResult.score;
        winDepth = minimaxDeeperResult.depth;
      }
      case WIN -> score = currentGameState == startGameState ? 10 : -10;
      case DRAW -> {
      }//TODO: odd case
      case AGAIN -> throw new ExceptionInInitializerError(); // TODO: What error?
    }
    return new MinimaxResult(possibleMove, score,
        currentGameState, winDepth);
  }

  private boolean positionCached(Position currentPosition, GameState currentGameState) {
    return minimaxCache.containsKey(currentPosition)
        && minimaxCache.get(currentPosition).state == currentGameState
        && currentPosition.isValid(minimaxCache.get(currentPosition).move);
  }

  protected record MinimaxResult(Move move, int score, GameState state, int depth) implements
      Comparable<MinimaxResult> {

    @Override
    public int compareTo(MinimaxResult o) {
        if (score != o.score) {
            return Integer.compare(score, o.score);
        }
      return Integer.compare(depth, o.depth);
    }

  }
}
