package shevchenko.hw2.players;

import shevchenko.hw2.GameState;
import shevchenko.hw2.Move;
import shevchenko.hw2.board.Position;

public abstract class Player {
    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public abstract Move move(GameState turn, Position position);

    public String getName() {
        return name;
    }
}
