package shevchenko.hw2.cell;

public class Cell implements Cloneable {

  private CellState state;

  public Cell(CellState state) {
    this.state = state;
  }

  public Cell() {
    this(CellState.E);
  }

  public void setState(CellState state) {
    this.state = state;
  }

  public boolean hasState(CellState state) {
    return this.state == state;
  }

  public boolean isEmpty() {
    return state == CellState.E;
  }

  @Override
  public String toString() {
    if (hasState(CellState.O)) {
      return "O";
    } else if (hasState(CellState.X)) {
      return "X";
    } else {
      return ".";
    }
  }

  @SuppressWarnings("MethodDoesntCallSuperMethod")
  @Override
  public Cell clone() {
    Cell clone = new Cell();
    clone.state = state;
    return clone;
  }
}
