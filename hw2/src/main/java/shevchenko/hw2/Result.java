package shevchenko.hw2;

public enum Result {
  CONTINUE, AGAIN, WIN, DRAW
}
