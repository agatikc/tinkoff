package shevchenko.hw2.board;

import shevchenko.hw2.GameState;
import shevchenko.hw2.Move;
import shevchenko.hw2.Result;
import shevchenko.hw2.cell.Cell;
import shevchenko.hw2.cell.CellState;

public interface Position {

  Result makeMoveAndChangePosition(Move move);

  Result analysePositionAndGetResult(CellState current);

  boolean isValid(Move move);

  Cell[][] getPositionAsArray();

  Move[] getPossibleMoves(GameState state);
}
