package players;

import game.GameState;
import game.Move;
import board.Position;

public abstract class Player {
    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public abstract Move move(GameState turn, Position position);

    public String getName() {
        return name;
    }
}
