package players;

import java.io.PrintStream;
import java.util.Scanner;
import game.GameState;
import game.Move;
import board.Position;

public class HumanPlayer extends Player{
    private final PrintStream out;
    private final Scanner in;

    public HumanPlayer(final String name, final Scanner in, final PrintStream out) {
        super(name);
        this.out = out;
        this.in = in;
    }
    public HumanPlayer(final String name) {
        this(name, new Scanner(System.in), System.out);
    }

    public HumanPlayer(final Scanner in, final PrintStream out) {
        this(in.nextLine(), in, out);
    }
    @Override
    public Move move(GameState turn, Position position) {
        out.println(getName() + "'s move.");
        out.println("Enter row and column");
        out.println(position);
        return new Move(in.nextInt(), in.nextInt(), turn);
    }
}
