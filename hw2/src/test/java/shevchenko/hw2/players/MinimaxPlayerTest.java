package shevchenko.hw2.players;

import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import shevchenko.hw2.GameState;
import shevchenko.hw2.Move;
import shevchenko.hw2.board.TicTacToeBoard;

public class MinimaxPlayerTest {

  public static MinimaxPlayer minimaxPlayer;


  @BeforeAll
  public static void setUp() {
    minimaxPlayer = new MinimaxPlayer("Computer 1");
  }

  @ParameterizedTest
  @MethodSource("winBoardsTestData")
  public void testWinMove(GameState state, TicTacToeBoard board, Move trueMove) {
    Move testMove = minimaxPlayer.move(state, board);
    Assertions.assertEquals(trueMove.getRow(), testMove.getRow());
    Assertions.assertEquals(trueMove.getColumn(), testMove.getColumn());
    Assertions.assertEquals(trueMove.getValue(), testMove.getValue());
  }

  public static Stream<Arguments> winBoardsTestData() {
    GameState firstTurn = new GameState();
    GameState secondTurn = firstTurn.clone();
    secondTurn.changeTurn();
    int[][] board3_1 = {
        {1, -1, 1},
        {-1, 1, -1},
        {0, 0, 0}
    };
    int[][] board3_2 = {
        {0, 0, 0},
        {-1, -1, 0},
        {0, 1, 1}
    };
    int[][] board3_3 = {
        {1, 0, -1},
        {1, -1, 1},
        {0, 0, 0}
    };
    int[][] board3_4 = {
        {1, 0, -1},
        {1, -1, 1},
        {0, -1, 1}
    };
    return Stream.of(
        Arguments.of(
            firstTurn,
            new TicTacToeBoard(board3_1),
            new Move(2, 0, firstTurn)),
        Arguments.of(
            firstTurn,
            new TicTacToeBoard(board3_2),
            new Move(2, 0, firstTurn)),
        Arguments.of(
            secondTurn,
            new TicTacToeBoard(board3_3),
            new Move(2, 0, secondTurn)),
        Arguments.of(
            secondTurn,
            new TicTacToeBoard(board3_3),
            new Move(2, 0, secondTurn))
    );
  }


}
