package shevchenko.hw2.board;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import shevchenko.hw2.cell.Cell;
import shevchenko.hw2.cell.CellState;

public class TicTacToeBoardTest {
  public static final Random RND_STATE = new Random();

  @ParameterizedTest
  @MethodSource("boardInitTestData")
  public void boardInitSizeTest(int mapSize, int[][] position, TicTacToeBoard board) {
    Assertions.assertEquals(mapSize, board.getSize());
    Assertions.assertEquals(position.length, board.getSize());
    for (int[] row: position) {
      Assertions.assertEquals(row.length, board.getSize());
    }
  }

  @ParameterizedTest
  @MethodSource("boardInitTestData")
  public void boardInitTest(int mapSize, int[][] position, TicTacToeBoard board) {
    Cell[][] boardState = board.getPositionAsArray();
    for (int i = 0; i < mapSize; i++) {
      for (int j = 0; j < mapSize; j++) {
        switch (position[i][j]) {
          case 1 -> Assertions.assertTrue(boardState[i][j].hasState(CellState.X));
          case -1 -> Assertions.assertTrue(boardState[i][j].hasState(CellState.O));
          default -> Assertions.assertTrue(boardState[i][j].hasState(CellState.E));
        }
      }
    }
  }

  public static Stream<Arguments> boardInitTestData() {
    return generateArgsFromSizesList(new int[]{2, 3, 5});
  }

  public static Stream<Arguments> generateArgsFromSizesList(int[] sizes) {
    return Arrays.stream(sizes).sequential().mapToObj(TicTacToeBoardTest::generateArgFromSize);
  }

  public static Arguments generateArgFromSize(int size) {
    int[][] position = generatePosition(size);
    return Arguments.of(size, position, new TicTacToeBoard(position));
  }

  public static int[][] generatePosition(int mapSize) {
    int[][] position = new int[mapSize][mapSize];
    for (int i = 0; i < mapSize; i++) {
      for (int j = 0; j < mapSize; j++) {
        position[i][j] = RND_STATE.nextInt(-1, 2);
      }
    }
    return position;
  }
}
