package ru.tinkoff.academy.tinkofflibrary.publisher;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.book.Book;

@Entity
@Table(name = "publisher")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Publisher {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "publisher_id", nullable = false)
  private Long publisherId;

  @Column(name = "publisher_name")
  private String name;

  @ManyToMany(mappedBy = "publishers")
  @JsonIgnoreProperties(value = "publishers")
  private Set<Book> books;

  @OneToOne
  @JoinColumn(name = "address_id")
  private Address address;
}
