package ru.tinkoff.academy.tinkofflibrary.genre;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tinkoff.academy.tinkofflibrary.book.Book;

@Entity
@Table(name = "genre")
@Builder
@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class Genre {

  @Id
  @GeneratedValue
  @Column(name = "genre_id", nullable = false)
  private Long genreId;

  @Column(name = "genre_name", nullable = false)
  private String genreName;

  @ManyToMany(mappedBy = "genres")
  @JsonIgnoreProperties(value = "genres")
  private List<Book> books;

}
