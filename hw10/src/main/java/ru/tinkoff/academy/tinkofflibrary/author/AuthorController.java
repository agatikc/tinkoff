package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @PostMapping
    public Author save(Author author) {
        return authorService.save(author);
    }

    @PutMapping
    public Author update(Author author) {
        return save(author);
    }

    @GetMapping
    public List<Author> findAll() {
        return authorService.findAll();
    }

    @GetMapping("/{authorId}")
    public Author getById(@PathVariable Long authorId) {
        return authorService.getById(authorId);
    }

    @DeleteMapping("/authorId")
    public void deleteById(@PathVariable Long authorId) {
        authorService.deleteById(authorId);
    }
}
