package ru.tinkoff.academy.tinkofflibrary.publisher;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.book.Book;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
}
