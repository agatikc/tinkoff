package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface GenreRepository extends JpaRepository<Genre, Long> {
}
