package ru.tinkoff.academy.tinkofflibrary.publisher;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "address_id")
  private Long addressId;

  @Column(name = "country")
  private String country;

  @Column(name = "city")
  private String city;

  @Column(name = "street")
  private String street;

  @Column(name = "home")
  private String home;

  @OneToOne(mappedBy = "address")
  private Publisher publisher;
}
