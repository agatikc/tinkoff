package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {

  private final GenreService genreService;

  @PostMapping
  public Genre save(Genre genre) {
    return genreService.save(genre);
  }

  @PutMapping
  public Genre update(Genre genre) {
    return genreService.save(genre);
  }

  @GetMapping("/{genreId}")
  public ResponseEntity<Genre> getById(@PathVariable Long genreId) {
    Optional<Genre> genre = genreService.findById(genreId);

    if (genre.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(genre.get(), HttpStatus.OK);
  }

  @GetMapping
  public List<Genre> getAll() {
    return genreService.findAll();
  }

  @DeleteMapping("/{genreId}")
  public void deleteById(@PathVariable Long genreId) {
    genreService.deleteById(genreId);
  }
}
