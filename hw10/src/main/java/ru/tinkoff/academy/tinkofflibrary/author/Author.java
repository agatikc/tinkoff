package ru.tinkoff.academy.tinkofflibrary.author;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import lombok.*;

import javax.persistence.*;
import ru.tinkoff.academy.tinkofflibrary.book.Book;


@Entity
@Table(name="author")
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Author {

    @Id
    @GeneratedValue
    @Column(name = "author_id", nullable = false)
    private Long authorId;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @ManyToMany(mappedBy = "authors")
    @JsonIgnoreProperties(value = "authors")
    private Set<Book> books;
}
