package ru.tinkoff.academy.tinkofflibrary.book;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

  private final BookService bookService;

  @GetMapping("/title")
  public Book getByTitle(@RequestParam String bookTitle) {
    return bookService.getByTitle(bookTitle);
  }

  @GetMapping
  public List<Book> getAll() {
    return bookService.findAll();
  }

  @GetMapping("/{bookId}")
  public ResponseEntity<Book> getById(@PathVariable Long bookId) {
    Optional<Book> book = bookService.findById(bookId);

    if (book.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(book.get(), HttpStatus.OK);
  }

  @GetMapping("/page/{pageNumber}")
  public List<Book> findBooksPageable(
      @PathVariable int pageNumber,
      @RequestParam(name = "size") int size
  ) {
    Pageable pageable = PageRequest.of(--pageNumber, size);

    return bookService.findBooksPageable(pageable).getContent();
  }


  @PostMapping
  public Book save(@RequestBody CreatingBookDto creatingBookDto) {
    return bookService.save(creatingBookDto);
  }

  @DeleteMapping("/{bookId}")
  public void delete(@PathVariable Long bookId) {
    bookService.remove(bookId);
  }

  @PutMapping
  public Book update(@RequestBody Book book) {
    return bookService.update(book);
  }
}
