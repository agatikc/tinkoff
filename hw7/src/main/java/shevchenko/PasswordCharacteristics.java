package shevchenko;

record PasswordCharacteristics (
    int passwordLength,
    boolean hasSpecialCharacters,
    boolean hasDigits) {
}