package shevchenko;

import java.util.Arrays;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;

public class UserInterface {
  //TODO: String const for Bundle && better init
  private static final ResourceBundle bundle = ResourceBundle.getBundle("shevchenko.RussianBundle");
  public static void main(String[] args) {
    PasswordCharacteristics characteristics = getUserPasswordCharacteristics(args);
    System.out.println(PasswordGenerator.generate(characteristics));
  }

  private static PasswordCharacteristics getUserPasswordCharacteristics(String[] args) {
    if(notOk(args)) {
      return askPasswordCharacteristics();
    }
    final int passwordLength = getPasswordLength(args);
    final boolean hasSpecialCharacters = getSpecialCharactersInfo(args);
    final boolean hasDigits = getDigitsInfo(args);
    return new PasswordCharacteristics(passwordLength, hasSpecialCharacters, hasDigits);
  }

  private static boolean notOk(String[] args) {
    return args != null && Arrays.stream(args).noneMatch(Objects::isNull) && args.length == 3;
  }

  private static PasswordCharacteristics askPasswordCharacteristics() {
    final Scanner in = new Scanner(System.in);
    final ConsoleUserInterface consoleUserInterface= new ConsoleUserInterface(in);
    final int passwordLength = consoleUserInterface.askPasswordLength();
    final boolean hasSpecialCharacters = consoleUserInterface.askSpecialCharactersInfo();
    final boolean hasDigits = consoleUserInterface.askDigitsInfo();
    in.close();
    return new PasswordCharacteristics(passwordLength, hasSpecialCharacters, hasDigits);
  }

  private static int getPasswordLength(String[] args) {
    return parsePasswordLength(args[0]);
  }

  private static boolean getSpecialCharactersInfo(String[] args) {
    return parseSpecialCharactersInfo(args[1]);
  }
  //TODO: select common parts for two console interfaces.
  private static boolean getDigitsInfo(String[] args) {
    return parseDigitsInfo(args[2]);
  }

  private static int parsePasswordLength(String lengthString){
    try {
      return Integer.parseInt(lengthString);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Can't parse length value: " + e.getMessage());
    }
  }

  private static boolean parseSpecialCharactersInfo(String specialCharactersInfoString) {
    return parseBooleanInfo(specialCharactersInfoString);
  }

  private static boolean parseDigitsInfo(String digitsInfoString) {
    return parseBooleanInfo(digitsInfoString);
  }

  private static boolean parseBooleanInfo(String booleanString) {
    //TODO: add info message about when true|false.
    return Boolean.parseBoolean(booleanString);
  }

  private record ConsoleUserInterface(Scanner in) {
    private String askInfo(String message) {
        System.out.println(message);
        String ans = in.nextLine();
        //System.out.println(ans);
        return ans;
      }

      private int askPasswordLength() {
        final String lengthString = askInfo(bundle.getString("password_length_ask"));
        return parsePasswordLength(lengthString);
      }

      private boolean askSpecialCharactersInfo() {
        final String specialCharsString = askInfo(bundle.getString("special_chars_ask"));
        return parseSpecialCharactersInfo(specialCharsString);
      }

      private boolean askDigitsInfo() {
        final String digitsInfo = askInfo(bundle.getString("digits_info_ask"));
        return parseDigitsInfo(digitsInfo);
      }
    }
}
