package shevchenko;

import java.util.ListResourceBundle;

public class RussianBundle extends ListResourceBundle {

  private static final Object[][] BUNDLE = {
      {"password_length_ask", "Введите длину пароля:"},
      {"special_chars_ask", "Должен ли пароль содержать специальные символы?"},
      {"digits_info_ask", "Должен ли пароль содержать цифры?"}
  };

  @Override
  protected Object[][] getContents() {
    return BUNDLE;
  }
}
