package shevchenko;

import java.util.Random;
import java.util.stream.IntStream;

public class NumbersGenerator {
  public static String getNiceNumberSequence(final int length) {
    final int trueLength = new Random().nextInt(length);
    int currLength = 0;
    StringBuilder sb = new StringBuilder();
    while (currLength < trueLength) {
      sb.append(currLength);
      currLength += String.valueOf(currLength).length();
    }
    return sb.toString();
  }
}
