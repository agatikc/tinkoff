package shevchenko;

import java.util.Random;

public enum GeneratingOption {
  WORD, WORD_WITH_SPECIAL_CHARS, NUMBER_SEQUENCE;
  private static final Random RND = new Random();
  public static GeneratingOption randomOption(PasswordCharacteristics characteristics)  {
    GeneratingOption[] options = values();
    return options[RND.nextInt(options.length)];
  }
}
