package shevchenko;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.CharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class WordsGenerator {
  private static final Random RND = new Random();
  private static final String WORDS_PATH = "C:\\Users\\agata\\IdeaProjects\\tinkoff\\hw7\\src\\main\\java\\shevchenko\\words";
  private static final String REPLACEMENT = "C:\\Users\\agata\\IdeaProjects\\tinkoff\\hw7\\src\\main\\java\\shevchenko\\replacement";
  private static final Map<String, String> REPLACEMENT_MAP;
  private static final List<String> WORDS;
  //private static final Map<Integer, List<String>> WORDS_MAP;
  static {
    try {
      WORDS = initWords();
      REPLACEMENT_MAP = initReplacementMap();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    //WORDS_MAP = initWordsMap();
  }

  private static Map<String, String> initReplacementMap() throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(REPLACEMENT));
    Map<String, String> replacement = new HashMap<>();
    String line;
    while ( (line = reader.readLine()) != null)
    {
      String[] pair = line.split(" : ");
      replacement.put(pair[0], pair[1]);
    }
    reader.close();
    return replacement;
  }

  private static List<String> initWords() throws IOException{
    BufferedReader reader = new BufferedReader(new FileReader(WORDS_PATH));
    List<String> wordList = new ArrayList<>();
    String line;
    while ( (line = reader.readLine()) != null)
    {
      wordList.addAll(List.of(line.split(" ")));
    }
    reader.close();
    return wordList;
  }
  public static String getRandomWord(final int maxLength) {
    List<String> wordsWithSmallerLength = WORDS.stream().filter(word -> word.length() <= maxLength).toList();
    return wordsWithSmallerLength.get((new Random()).nextInt(0, wordsWithSmallerLength.size()));
    //TODO: EmptyString or throwing error
  }
  public static String getRandomWordWithSpecialChars(final int maxLength) {
    StringBuilder sb = new StringBuilder();
    getRandomWord(maxLength).chars().forEach(ch ->
      sb.append(RND.nextBoolean() ? REPLACEMENT_MAP.get(String.valueOf(ch)) : ch));
    return sb.toString();
  }

}
