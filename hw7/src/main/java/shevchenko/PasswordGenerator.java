package shevchenko;

import java.util.Random;

public class PasswordGenerator {
  private static final String SYMBOLS = "!?(){}|\\/-^$.,";

  public static String generate(PasswordCharacteristics characteristics) {
    final PasswordCollector password = new PasswordCollector(characteristics.passwordLength());
    if (characteristics.hasSpecialCharacters()) {
      password.appendSymbol();
    }
    if (characteristics.hasDigits()){
      password.appendNumberSequence();
    }
    while (password.isNotDone()) {
      GeneratingOption currOption = GeneratingOption.randomOption(characteristics);
      switch (currOption) {
        case WORD, WORD_WITH_SPECIAL_CHARS -> password.appendWord();
        case NUMBER_SEQUENCE -> password.appendNumberSequence();
      }
    }
    return password.toString();
  }

  private static class PasswordCollector {
    private final StringBuilder password;
    private int remainingLength;
    PasswordCollector(int startRemainingLength) {
      password = new StringBuilder();
      remainingLength = startRemainingLength;
    }

    public boolean isNotDone() {
      return !isDone();
    }

    public boolean isDone() {
      return remainingLength <= 0;
    }

    public void appendWord() {
      appendPartOfPassword(generateWord());
    }

    public void appendWordWithSpecialChars() {
      appendPartOfPassword(generateWordWithSpecialChars());
    }

    public void appendNumberSequence() {
      appendPartOfPassword(generateNumberSequence());
    }

    private void appendPartOfPassword(String partOfPassword) {
      password.append(partOfPassword);
      remainingLength -= partOfPassword.length();
    }
    private String generateNumberSequence() {
      return NumbersGenerator.getNiceNumberSequence(remainingLength);
    }

    private String generateWord() {
      return WordsGenerator.getRandomWord(remainingLength);
    }

    private String generateWordWithSpecialChars() {
      return WordsGenerator.getRandomWordWithSpecialChars(remainingLength);
    }

    @Override
    public String toString() {
      return password.toString();
    }

    public void appendSymbol() {
      char ch = SYMBOLS.charAt((new Random()).nextInt(SYMBOLS.length()));
      String randomSymbol = String.valueOf(ch);
      appendPartOfPassword(randomSymbol);
    }
  }
}
